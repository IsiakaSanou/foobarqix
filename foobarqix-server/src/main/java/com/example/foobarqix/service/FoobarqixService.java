package com.example.foobarqix.service;

import com.example.foobarqix.entity.HistoryEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class FoobarqixService {

    private final HistoryService historyService;

    public FoobarqixService(HistoryService historyService) {
        this.historyService = historyService;
    }

    public String computeAndSave(int input) {
        String result = getTranslation(input);

        historyService.save(HistoryEntity.builder().input(input).result(result).build());

        return result;
    }

    private static String getTranslation(int input) {
        StringBuilder result = new StringBuilder();

        result.append(getDivisibleTranslation(input));
        result.append(getStringTranslation(String.valueOf(input)));

        if (result.isEmpty()) {
            return String.valueOf(input);
        }
        return result.toString();
    }

    private static String getDivisibleTranslation(int input) {
        StringBuilder result = new StringBuilder();
        if (input % 3 == 0) {
            result.append("Foo");
        }
        if (input % 5 == 0) {
            result.append("Bar");
        }
        if (input % 7 == 0) {
            result.append("Qix");
        }
        return result.toString();
    }

    private static String getStringTranslation(String input) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < input.length(); i++) {
            result.append(getCharTranslation(input.charAt(i)));
        }
        return result.toString();
    }

    private static String getCharTranslation(char c) {
        switch (c) {
            case '3':
                return "Foo";
            case '5':
                return "Bar";
            case '7':
                return "Qix";
        }
        return "";
    }
}
