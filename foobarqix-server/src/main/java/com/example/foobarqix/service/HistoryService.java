package com.example.foobarqix.service;

import com.example.foobarqix.entity.HistoryEntity;
import com.example.foobarqix.repository.HistoryRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class HistoryService {

    private final HistoryRepository repository;

    public HistoryService(HistoryRepository repository) {
        this.repository = repository;
    }

    public HistoryEntity save(HistoryEntity history) {
        return repository.save(history);
    }

    public List<HistoryEntity> getAll() {
        return repository.findAll();
    }

}
