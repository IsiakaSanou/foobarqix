package com.example.foobarqix.controller;

import com.example.foobarqix.service.FoobarqixService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:8081")
@RestController
@RequestMapping("/api/foobarqix")
public class FoobarqixController {

    private final FoobarqixService service;

    public FoobarqixController(FoobarqixService service) {
        this.service = service;
    }

    @GetMapping("/{input}")
    public String compute(@PathVariable int input) {
        return service.computeAndSave(input);
    }
}
