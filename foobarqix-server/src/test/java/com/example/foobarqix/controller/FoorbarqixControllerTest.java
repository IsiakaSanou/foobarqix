package com.example.foobarqix.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class FoorbarqixControllerTest {

    @Autowired
    private MockMvc mvc;

    private static final String FOOBARQIX_API = "/api/foobarqix/";

    @Test
    @Transactional
    void should_get_a_compute_value() throws Exception {
        // WHEN + THEN
        mvc.perform(get(FOOBARQIX_API + "3"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", is("FooFoo")));
    }

}
