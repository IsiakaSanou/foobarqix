package com.example.foobarqix.controller;

import com.example.foobarqix.entity.HistoryEntity;
import com.example.foobarqix.service.HistoryService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class HistoryControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private HistoryService historyService;

    private static final String HISTORY_API = "/api/history/";

    @Test
    @Transactional
    void should_get_a_history() throws Exception {
        // GIVEN
        historyService.save(HistoryEntity.builder().input(1).result("1").build());
        historyService.save(HistoryEntity.builder().input(2).result("2").build());
        historyService.save(HistoryEntity.builder().input(3).result("FooFoo").build());

        // WHEN + THEN
        mvc.perform(get(HISTORY_API))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].input", is(1)))
                .andExpect(jsonPath("$[1].input", is(2)))
                .andExpect(jsonPath("$[2].input", is(3)));
    }

}
