package com.example.foobarqix.repository;

import com.example.foobarqix.entity.HistoryEntity;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
class HistoryRepositoryTest {

    @Autowired
    private HistoryRepository historyRepository;

    @Test
    public void should_save_all_history() {
        // GIVEN
        HistoryEntity h1 = HistoryEntity.builder().input(1).result("1").build();
        HistoryEntity h2 = HistoryEntity.builder().input(2).result("2").build();
        HistoryEntity h3 = HistoryEntity.builder().input(3).result("FooFoo").build();

        // WHEN
        historyRepository.saveAll(List.of(h1, h2, h3));

        // THEN
        List<HistoryEntity> history = historyRepository.findAll();

        assertThat(history).extracting("input").contains(1, 2, 3);
    }
}
