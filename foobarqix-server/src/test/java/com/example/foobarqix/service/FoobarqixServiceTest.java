package com.example.foobarqix.service;

import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class FoobarqixServiceTest {

    @InjectMocks
    private FoobarqixService foobarqixService;

    @Mock
    private HistoryService historyService;

    @ParameterizedTest
    @CsvSource({"1,1", "2,2", "3,FooFoo", "4,4", "5,BarBar", "6,Foo", "7,QixQix", "8,8", "9,Foo", "10,Bar",
            "13,Foo", "15,FooBarBar", "21,FooQix", "33,FooFooFoo", "51,FooBar", "53,BarFoo"})
    public void should_return_correct_result(int input, String expectedResult) {
        assertThat(foobarqixService.computeAndSave(input)).isEqualTo(expectedResult);
    }

}
