package com.example.foobarqix.service;

import com.example.foobarqix.entity.HistoryEntity;
import com.example.foobarqix.repository.HistoryRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class HistoryServiceTest {

    @InjectMocks
    private HistoryService service;

    @Mock
    private HistoryRepository historyRepository;

    @Test
    public void should_save_entity() {
        // GIVEN
        HistoryEntity h1 = HistoryEntity.builder().input(1).result("1").build();
        when(historyRepository.save(h1)).thenReturn(h1);

        // WHEN
        historyRepository.save(h1);

        // THEN
        verify(historyRepository).save(h1);
    }

    @Test
    public void should_return_all_history_entity() {
        // GIVEN
        HistoryEntity h1 = HistoryEntity.builder().input(1).result("1").build();
        HistoryEntity h2 = HistoryEntity.builder().input(2).result("2").build();
        HistoryEntity h3 = HistoryEntity.builder().input(3).result("FooFoo").build();
        when(historyRepository.findAll()).thenReturn(List.of(h1, h2, h3));

        // WHEN
        List<HistoryEntity> history = service.getAll();

        // THEN
        assertThat(history).extracting("input").contains(1, 2, 3);

    }

}
